export const fetchUsers = async () => {
  const response = await fetch('https://662e5cd4a7dda1fa378cc28a.mockapi.io/users/users');

  if (!response.ok) {
    throw new Error('Network response was not ok')
  }

  return response.json()
}