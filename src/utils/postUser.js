export const postUsers = async (user) => {
  const response = await fetch('https://662e5cd4a7dda1fa378cc28a.mockapi.io/users/users', {
    method: 'POST',
    headers: {
      'Content-type': 'application/json; charset=UTF-8',
    },
      body: JSON.stringify(user),
  });

  if (!response.ok) {
    throw new Error('Network response was not ok')
  }

  return response.json()
}