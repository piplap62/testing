import {fetchUsers} from './fetchUsers';

describe('fetchUsers', () => {
  test('fetchUsers should response ok', async () => {
    global.fetch = jest.fn(() => (
      Promise.resolve({
        ok: true,
        json: ()=> Promise.resolve([{id: 1, name: 'kek', userName: 'kekov'}])
      })
    ));

    const users = await fetchUsers();

    expect(users).toHaveLength(1);
    expect(users[0]).toEqual({id: 1, name: 'kek', userName: 'kekov'});
  });
})