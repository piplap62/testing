import { UserForm } from './components/UserForm/UserForm'
import {Grid, Container} from '@mui/material';
import { useQuery } from 'react-query'
import { fetchUsers } from './utils/fetchUsers'
import UserList from './components/UserList/UserList'

function App() {
  const {data, isLoading} = useQuery('users', fetchUsers, {
    refetchOnWindowFocus: false,
  });

  return (
    <Container maxWidth='md'>
      <UserForm />
      {isLoading && <span>Loading</span>}
      <Grid container >
        <UserList data={data}/>
      </Grid>
    </Container>  
  );
}

export default App;
