import React from 'react';
import { Formik, Form, useField } from 'formik';
import {Button, TextField, Box} from '@mui/material';
import {useMutation, useQueryClient} from 'react-query';
import {postUsers} from '../../utils/postUser';

const validate = ({name, userName}) => {
  const errors = {};

  if(!name) {
    errors.name = 'Required';
  } else if (name.length < 3) {
    errors.name = 'Must be more then 3 letters';
  }

  if (userName && userName.length < 3) {
    errors.userName = 'Must be more then 3 letters';
  }

  return errors;
}

const Input = ({label, ...props}) => {
  const [field, meta] = useField(props);
  
  const isError = meta.touched && Boolean(meta.error);

  return(
    <>
      <TextField
        variant='filled'
        size="small"
        style={{width: '100%'}}
        error={isError}
        helperText={isError && meta.error}
        label={label}
        value={field.value}
        onChange={field.onChange}
        onBlur={field.onBlur}
        {...props}
      />
    </>
  )
}

export const UserForm = () => {
  const queryClient = useQueryClient();

  const createPostMutation = useMutation(postUsers, {
    onSuccess: () => queryClient.invalidateQueries('users')
  });

  return (
    <Formik
      initialValues = {{
        name: '',
        userName: '',
      }}
      onSubmit={async (values, {setSubmitting, resetForm}) => {
        await createPostMutation.mutateAsync({id: 1, ...values});
        setSubmitting(false);
        resetForm();
      }}
      validate={validate}
    >
      { ({errors, isSubmitting}) => {
        return(
          <Form noValidate>
            <Box sx={{minHeight: '4.5rem'}}>
              <Input
                id="name"
                name="name"
                type="text"
                label="name"
                placeholder="Enter your first name"
                required
              />
            </Box>
            <Box sx={{minHeight: '4.5rem'}}>
              <Input
                id="userName"
                name="userName"
                type="text"
                label="userName"
                placeholder="Enter your last name"
              />
            </Box>
            <Box display={'flex'} justifyContent={'center'}>
              <Button variant='contained' type="submit">Submit</Button>
            </Box>  
          </Form>
        )
      }
      }
    </Formik>
  );
}