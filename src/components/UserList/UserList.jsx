import {List, ListItem, useMediaQuery} from '@mui/material';
import { memo } from 'react';

const UserList = ({data}) => {
  const isMiniTab= useMediaQuery('(max-width:865px)');
  const isMobile = useMediaQuery('(max-width:560px)');

  return (
    <>  
      {data && 
        <List sx={{ ...(isMobile & {margin: 'auto'}), ...(isMiniTab ? {width: '40rem', margin: 'auto'} : {width: '100%'}), display: 'flex', flexWrap: 'wrap', ...(isMiniTab && {justifyContent: 'space-between',})}}>
          {data.map(({id, name, userName}, index) => {
            return <ListItem
              sx={{
                maxWidth: '16rem',
                ...(index % 3 === 1 && !isMiniTab && {margin: '0 1rem 0 1rem'}),
                justifyContent: 'space-between',
                border: '1px solid black',
                mb: '1rem',
              }}
              key={id}
            >
            <p style={{margin: 0, maxWidth: '100%', overflowWrap: 'break-word' }}>
              {name} {userName}
            </p>
            </ListItem>
          })}
        </List>
      }
    </>
  )
};

export default memo(UserList);